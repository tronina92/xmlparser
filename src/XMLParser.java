import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class XMLParser {
    static JFrame frame;

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        frame = new JFrame();
        JFileChooser openFile = new JFileChooser();
        openFile.showOpenDialog(frame);
        File file = openFile.getSelectedFile();


        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);

        NodeList customers = document.getElementsByTagName("Customer");
        List<Customer> customerList = new ArrayList<>();

        NodeList addresses = document.getElementsByTagName("FullAddress");


        for (int i=0; i<customers.getLength(); i++) {
            if (customers.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element customerElement = (Element) customers.item(i);
                Element addressElement = (Element) addresses.item(i);

                Customer customer = new Customer();
                customer.setCustomerID(String.valueOf(customerElement.getAttribute("CustomerID")));

                Customer.FullAddress fullAddress = new Customer.FullAddress();
                NodeList childNodesAddress = addressElement.getChildNodes();
                for (int j=0; j<childNodesAddress.getLength();j++){
                    if (childNodesAddress.item(j).getNodeType() == Node.ELEMENT_NODE) {
                        Element childNodeElementAddress = (Element) childNodesAddress.item(j);
                        switch (childNodeElementAddress.getNodeName()) {
                            case "Address" :{
                                customer.getFullAddress().setAddress(childNodeElementAddress.getTextContent());
                            } break;
                            case "City" : {
                                customer.getFullAddress().setCity(childNodeElementAddress.getTextContent());
                            } break;
                            case "Region" :{
                                customer.getFullAddress().setRegion(childNodeElementAddress.getTextContent());
                            } break;
                            case "PostalCode" : {
                                customer.getFullAddress().setPostalCode(Integer.valueOf(childNodeElementAddress.getTextContent()));
                            } break;
                            case "Country" : {
                                customer.getFullAddress().setCountry(childNodeElementAddress.getTextContent());
                            } break;
                        }

                    }
                }

                NodeList childNodes = customerElement.getChildNodes();
                for (int j=0; j<childNodes.getLength(); j++) {
                    if (childNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
                        Element childElement = (Element) childNodes.item(j);

                        switch (childElement.getNodeName()) {
                            case "CompanyName": {
                                customer.setCompanyName(childElement.getTextContent());
                            } break;
                            case "ContactName": {
                                customer.setContactName(childElement.getTextContent());
                            } break;
                            case "Phone": {
                                customer.setPhone(childElement.getTextContent());
                            } break;

                        }
                    }
                }
                customerList.add(customer);
            }
        }
        String text = "";

        String title = "CustomerID / CompanyName / ContactName / Phone Address / City Region / PostalCode / Country ";

        for(Customer c:customerList) text = text + c.toString() + "<br>";
        System.out.println(text);

        JTextPane area = new JTextPane();
        area.setContentType("text/html");
        area.setText("<html><center><b>" + title + "</b><br>" + text + "</center></html>");
        frame.add(area);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800,200);
        frame.setVisible(true);
    }

}

class Customer {
    private String customerID;
    private String companyName;
    private String contactName;
    private String phone;
    private FullAddress fullAddress = new FullAddress();

    public String getCustomerID() {
        return customerID;
    }

    @Override
    public String toString() {
        return customerID + " / " + companyName + " / "  + contactName + " / "  + phone + " / "  + fullAddress;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public FullAddress getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(FullAddress fullAddress) {
        this.fullAddress = fullAddress;
    }

    public static class FullAddress {
        private String address;
        private String city;
        private String region;
        private Integer postalCode;
        private String country;

        public String getAddress() {
            return address;
        }

        @Override
        public String toString() {
            return address + " / "  +city + " / "  + region + " / "  + postalCode + " / "  +country;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public Integer getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(Integer postalCode) {
            this.postalCode = postalCode;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }
    }



}
